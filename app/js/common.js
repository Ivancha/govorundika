$(document).ready(function(){
  $('.slider').slider({
    height: 600
  });
  $('.carousel').carousel({
    dist: -20,
    padding: 150
  });
   $('.dropdown-button').dropdown({
     hover: true,
     belowOrigin: true,
     constrainWidth: false
   });
  $('.card').matchHeight();
});
